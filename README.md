# CS207 Final Project
BME 547 CPAP Measurements Assignment

## Team

1. Lucas Gibb: “lpg10”
2. Asfandyar Khan: “ak556”
3. Christian Garvin: “cgg18”

## Description
Given that we are avid music listeners and even have our very own DJ within the group (shoutout Asfi), we would love to create an app that is all centered around the Apple Music functionality. 
This app ideally would be able to develop a curated playlist depending on several factors, such as time of day, vibe (taking in factors such as weather for a more interesting result) , and type of event. 
The app would then generate a specific API call based on those factors and create a playlist for the user.
We are also thinking of using external API’s to help us sift through the music based on details like bpm, key signature of the song etc. 
Getting really ambitious here, we could then have the user submit a form after they were done playing the music to see how much they liked the curated music, and then the app can adjust the user’s preferences for the future.
We are still figuring out what level of complexity we would want our app to have, and will tweak the API calls and functional algorithms depending on the functionality we decide to move forward with. 

#### Specific iOS/API technologies we will use
1. MusicKit
2. API Calls
3. Forms
4. DataStore
5. Lists and Arrays
6. Buttons

#### Sources of complexity/difficulty
We expect difficulties in specific/different API calls based on different factors that the user can control. 
We also want to have the app being able to learn from user feedback and improve itself through our implemented algorithm,
which may prove challenging. 



## A rough description of the vision behind our app, the flow, and its functionality:
Music has the power to make the worst and best moments of our life feel much better. The vision behind our project is to leverage the power of music, to further deepen connections, make new connections, and to help take away the pain of deciding who gets to be on the aux. 
Leveraging the Apple MusicKit, we hope to use our matching algorithm to curate a specific shared playlist for our users. We will further tweak the kind of playlist created based on inputs the users will provide us, for example if our users are in a relationship and need a playlist to set the tone for their date, they can click the date button and our algorithm will whip up a playlist relevant to the “mood”.
Similarly, for a small dorm party, we can take the playlists of our participants and filter out songs from their libraries based on their “hype” levels. Then using the most “hype” songs from each individual to formulate a shared mega party playlist. This way everyone gets to hear new music, dance to cult classics, and not fret about the lack of energy.
We are also thinking along the lines of using weather and time, to curate specific playlists for individuals. For example, on a beautiful spring morning the “mood” button when clicked will create a vibrant and soft playlist that fits the tone of the day, using songs from the users libraries. These are two similar ideas, yet with important differences. 
Our job now is to understand and lock into the functionality we think would be most beneficial for our users. In the process of our work, we hope to find a way to unify the ideas and bring both functionalities to our users. 

## Jon Comments/Approval 3/17/23
* Looks good!
* Crush it!

## Jon Storyboard Approval 3/27/23



