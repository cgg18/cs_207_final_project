//
//  AppleMusicApi.swift
//  MixMatch
//
//  Created by Asfandyar Khan on 4/4/23.
//

// LastFMAPI.swift
import Foundation

class LastFMAPI {
    private let apiKey = "27434eda45ff417037c216039dc742e4"
    private let baseUrl = "https://ws.audioscrobbler.com/2.0/"
    private let session = URLSession.shared
    
    func searchSongs(songName: String, completion: @escaping ([Song]?) -> Void) {
        let encodedSongName = songName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(baseUrl)?method=track.search&track=\(encodedSongName)&api_key=\(apiKey)&format=json")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let apiResponse = try decoder.decode(APIResponse.self, from: data)
                let songs = apiResponse.results.trackmatches.track.map { trackData -> Song in
                    return Song(
                        id: trackData.url,
                        name: trackData.name,
                        artist: trackData.artist,
                        genre: trackData.toptags.tag.map({ $0.name }).joined(separator: ", "),
                        coverUrl: URL(string: trackData.image.first(where: { $0.size == "large" })?.text ?? ""),
                        bpm: trackData.toptags.tag.first(where: { $0.name.lowercased() == "bpm" })?.count ?? 0,
                        popularity: Int(trackData.listeners) ?? 0,
                        key: ""
                    )
                }
                completion(songs)
            } catch {
                print("Error decoding JSON: \(error)")
                completion(nil)
            }
        }
        task.resume()
    }
}
