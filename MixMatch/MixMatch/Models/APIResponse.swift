//
//  APIResponse.swift
//  MixMatch
//
//  Created by Asfandyar Khan on 4/4/23.
//

struct APIResponse: Decodable {
    let results: TrackResults

    struct TrackResults: Decodable {
        let trackmatches: TrackMatches

        struct TrackMatches: Decodable {
            let track: [Track]

            struct Track: Decodable {
                let name: String
                let artist: String
                let url: String
                let image: [Image]
                let listeners: String
                let toptags: TopTags

                struct Image: Decodable {
                    let text: String
                    let size: String
                }

                struct TopTags: Decodable {
                    let tag: [Tag]

                    struct Tag: Decodable {
                        let name: String
                        let count: Int?
                    }
                }
            }
        }
    }
}
