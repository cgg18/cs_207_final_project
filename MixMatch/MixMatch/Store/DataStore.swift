//
//  DataStore.swift
//  MixMatch
//
//  Created by Asfandyar Khan on 4/4/23.
// Lucas was here
// Garv was here

import Foundation
import SwiftUI
import Combine

class DataStore: ObservableObject {
    @Published var users: [User] = User.previewData
    @Published var currentUsername: String = ""
    @Published var mixMatchFriends: [User] = []
    //@Published var newPlaylist: Playlist = Playlist //to add playlist for MixMatchView
    @Published var selectedSongs: [Song] = []
    
    func getCurrentUser() -> User? {
        return users.first(where: { $0.username.lowercased() == currentUsername.lowercased() })
    }
    
    func getOtherUser(username: String) -> User? {
        if let index = users.firstIndex(where: { $0.username.lowercased() == username.lowercased() }){
            return users[index]
        }else{
            return nil
        }
    }
    
    func addUserToFriendsList(_ user: User){
        if let index = users.firstIndex(where: { $0.username.lowercased() == currentUsername.lowercased() }) {
            var thisUser = users[index]
            thisUser.friends.append(user)
            users[index] = thisUser
        }
    }
    
    func removeUserFromFriendsList(_ user: User){
        if let userIndex = users.firstIndex(where: {$0.username == currentUsername}){
            var thisUser = users[userIndex]
            if let friendIndex = thisUser.friends.firstIndex(where: { $0.id.lowercased() == user.id.lowercased() }) {
                thisUser.friends.remove(at: friendIndex)
                users[userIndex] = thisUser
            }
        }
    }
    func userIsFriend(_ user: User) -> Bool{
        if getCurrentUser()!.friends.firstIndex(where: { $0.username.lowercased() == user.username.lowercased() }) != nil{
            return true
        } else if getCurrentUser() == nil{
            return false
        }
        else{
            return false
        }
    }
    
    func addNewUser(_ user: User){
        currentUsername = user.username
        users.append(user)
    }
    
    func signInUser(_ username: String){
        currentUsername = username
    }
    
    func signOutUser(){
        currentUsername = ""
    }
    
    func getPassword(_ username: String) -> String{
        let user = users.first(where: {$0.username.lowercased() == username})
        if user == nil{
            return ""
        }
        return user!.password
    }
    
    func addUserToMixMatch(_ user: User){
        mixMatchFriends.append(user)
    }
    
    func removeUserFromMixMatch(_ user: User){
        if let friendIndex = mixMatchFriends.firstIndex(where: { $0.id.lowercased() == user.id.lowercased() }) {
            mixMatchFriends.remove(at: friendIndex)
        }
    }
    
    func userIsInMixMatch(_ user: User) -> Bool{
        if mixMatchFriends.firstIndex(where: { $0.username.lowercased() == user.username.lowercased() }) != nil{
            return true
        }
        else{
            return false
        }
    }
    
    ///
    func songIsInSelfPlaylist(_ song: Song) -> Bool{
        if selectedSongs.firstIndex(where: { $0.name == song.name }) != nil{
            return true
        }
        else{
            return false
        }
    }
    
    func addSongToSelfPlaylist(_ song: Song){
        selectedSongs.append(song)
    }
    
    func removeSongSelfPlaylist(_ song: Song){
        if let songIndex = selectedSongs.firstIndex(where: { $0.name == song.name }) {
            selectedSongs.remove(at: songIndex)
        }
    }
    
    ////
    func generatePlaylist(songs: [Song], mood: MoodCategory) -> [Song] {
        let filteredSongs = filterSongs(songs: songs, mood: mood)
        let sortedSongs = sortSongs(songs: filteredSongs, mood: mood)
        
        return sortedSongs
    }

    func filterSongs(songs: [Song], mood: MoodCategory) -> [Song] {
        switch mood {
        case .party:
            return songs.filter { ($0.bpm ?? 0) >= 120 && $0.key.contains("major") }
        case .chill:
            return songs.filter { ($0.bpm ?? 0) < 120 && $0.key.contains("minor") }
        case .workout:
            return songs.filter { ($0.bpm ?? 0) >= 130 }
        }
    }

    func sortSongs(songs: [Song], mood: MoodCategory) -> [Song] {
        switch mood {
        case .party:
            return songs.sorted { ($0.popularity ?? 0) > ($1.popularity ?? 0) }
        case .chill:
            return songs.sorted { ($0.bpm ?? 0) < ($1.bpm ?? 0) }
        case .workout:
            return songs.sorted { ($0.bpm ?? 0) > ($1.bpm ?? 0) }
        }
    }
    
    func mixMatch(_ playlistName: String, _ mood: MoodCategory, _ thisUser: User){
        var allSongsInitial: [Song] = []
        //var allSongs: Set<Song> = [] //makes it so that allSongs is unique and there are no repeats
        
        mixMatchFriends.append(thisUser)
        
        for user in mixMatchFriends {
            for playlist in user.playlists.filter({$0.mixedMatchedBy.isEmpty}){ //line changed by Lucas for Garv
                for song in playlist.songs{
                    allSongsInitial.append(song)
                }

            }
      }
        let setOfSongs = Set(allSongsInitial) // create a set from the array to remove duplicates

        let allSongs = Array(setOfSongs) // convert the set back to an array
        
        let newPlaylistSongs = generatePlaylist(songs: allSongs, mood: mood)
        let newPlaylist = Playlist(name: playlistName, songs: newPlaylistSongs, mixedMatchedBy: thisUser.username)

        for sharedUser in mixMatchFriends{
            addPlaylistToUser(sharedUser, newPlaylist)
        }
        mixMatchFriends = []
        


    } //end of MixMatch
    
    func addPlaylistToUser(_ user: User, _ newPlaylist: Playlist){
        if let index = users.firstIndex(where: { $0.username.lowercased() == user.username.lowercased() }) {
            var thisUser = users[index]
            thisUser.playlists.append(newPlaylist)
            users[index] = thisUser
        }
    }
}

