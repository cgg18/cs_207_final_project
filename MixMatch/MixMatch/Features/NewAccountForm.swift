//
//  NewAccountForm.swift
//  MixMatch
//
//  Created by Christian Garvin on 4/24/23.
//

import SwiftUI

struct NewAccountForm: View {
    @EnvironmentObject var dataStore: DataStore
    @State private var username: String = ""
    @State private var name: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State private var isCreateFailed: Bool = false
    @State private var failureMessage: String = ""
    @Binding var isPresented: Bool
    @Binding var user: User?
    
    var body: some View {
        Form {
            Section(header: Text("Create an Account")
                .foregroundColor(.black).font(.title2)) {

                    TextField("Username", text: $username)
                    TextField("Name", text: $name)
                    SecureField("Password", text: $password)
                    SecureField("Confirm Password", text: $confirmPassword)
            }
            
            Section {
                Button(action: {
                    let existing = dataStore.getPassword(username.lowercased())
                    if existing != ""{
                        isCreateFailed.toggle()
                        failureMessage = "Username Already Exists"
                    } else if password != confirmPassword{
                        isCreateFailed.toggle()
                        failureMessage = "Password and confirmed password do not match"
                    } else{
                        let newUser = User(username: username.trimmingCharacters(in: .whitespacesAndNewlines), password: password.trimmingCharacters(in: .whitespacesAndNewlines), name: name.trimmingCharacters(in: .whitespacesAndNewlines))
                        dataStore.addNewUser(newUser)
                        user = dataStore.getCurrentUser()
                        isPresented.toggle()
                    }
                }) {
                    HStack {
                        Spacer()
                        Text("Create Account")
                        Spacer()
                    }
                }.disabled(username.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || confirmPassword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
            }
        }
        .alert(isPresented: $isCreateFailed) {
            Alert(title: Text("Account Creation Failed"),
                  message: Text(failureMessage),
                  dismissButton: .default(Text("OK")))
        }
    }
}
