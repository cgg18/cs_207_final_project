////
////  testingviews.swift
////  MixMatch
////
////  Created by Asfandyar Khan on 4/25/23.
////
//
//import SwiftUI
//import Foundation
//import AVFoundation
//
//struct TestingViews: View {
//    @State private var songName = ""
//    @State private var songs: [Song] = []
//    @State private var isLoading = false
//    private let player = AVPlayer()
//    
//    var body: some View {
//        VStack {
//            TextField("Enter song name", text: $songName)
//                .textFieldStyle(RoundedBorderTextFieldStyle())
//                .padding()
//            
//            Button(action: {
//                fetchSongDetails()
//            }) {
//                Text("Search")
//            }
//            .padding()
//            
//            if isLoading {
//                ProgressView()
//                    .progressViewStyle(CircularProgressViewStyle())
//            } else if songs.isEmpty {
//                Text("No results found")
//                    .font(.headline)
//                    .foregroundColor(.secondary)
//                    .padding()
//            } else {
//                List(songs) { song in
//                    NavigationLink(destination: SongDetailsView(song: song)) {
//                        HStack {
//                            VStack(alignment: .leading) {
//                                Text(song.name)
//                                    .font(.headline)
//                                Text(song.artist ?? "")
//                                    .font(.subheadline)
//                            }
//                            Spacer()
//                            Image(systemName: "play.circle.fill")
//                                .resizable()
//                                .aspectRatio(contentMode: .fit)
//                                .frame(width: 32, height: 32)
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    func fetchSongDetails() {
//        songs = []
//        isLoading = true
//        let api = LastFMAPI()
//        api.searchSongs(songName: songName) { songs in
//            DispatchQueue.main.async {
//                isLoading = false
//                self.songs = songs ?? []
//            }
//        }
//    }
//    
//    
//    struct TestingViews_Previews: PreviewProvider {
//        static var previews: some View {
//            TestingViews()
//                .environmentObject(DataStore())
//        }
//    }
//    
//    struct SongDetailsView: View {
//        let song: Song
//        
//        var body: some View {
//            VStack {
//                Text(song.name)
//                Text(song.artist ?? "")
//            }
//        }
//    }
//    
//}
//
