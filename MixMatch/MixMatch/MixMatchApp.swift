//
//  MixMatchApp.swift
//  MixMatch
//
//  Created by Christian Garvin on 3/27/23.
//

import SwiftUI

@main
struct MixMatchApp: App {
    @StateObject var dataStore = DataStore()

    var body: some Scene {
        WindowGroup {
            MainTabView()
                .environmentObject(dataStore)
        }
    }
}
